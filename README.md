# Loco

Loco is a tiny webapp makes a best-effort attempt to determine the client's current location.

## Usage

Loco is built using maven:

    $ mvn package

The application can now be found in target/loco.jar

Once built, the application can be run with the following command

    $ java -jar loco.jar server loco.yml

## Options

Loco requires valid Factual API credentials to run, which may be provided at the command line. Unless otherwise
specified, the application and admin functionality will run on port 8080.

## Examples

An example incantation with all the options specified is in the Procfile, and looks like this:

    $ java -Ddw.http.port=5000 -Ddw.http.adminPort=5000 -Ddw.factual.key=key -Ddw.factual.secret=secret -jar target/loco.jar server loco.yml

## License

Copyright © Dimas Guardado, Jr. All rights reserved.
