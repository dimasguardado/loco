/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
(function (nav) {

  if (nav.geolocation) {
    return;
  }

  var loc;
  var pub = {};

  pub.getCurrentPosition = function(callback) {

    if (loc) {
      callback(loc);
      return;
    }

    if (google.loader.ClientLocation) {
      loc = {
        coords : {
          latitude: google.loader.ClientLocation.latitude,
          longitude: google.loader.ClientLocation.longitude
        }
       };
    }

    callback(loc);
  }

  pub.watchPosition = pub.getCurrentPosition;
  nav.geolocation = pub;

})(navigator);

(function ($, geo) {

  geo.watchPosition(function(pos) {
    $.get("/service/location", {lat: pos.coords.latitude, long: pos.coords.longitude}, function(location) {
        $(document).ready(function() {
            $("#location").html(location.address + " (" +
                                location.lat + ", " +
                                location.long + ")");
        });
    }, "json");
  });

})(jQuery, navigator.geolocation);



