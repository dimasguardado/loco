/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
package com.humandriven.loco

import javax.ws.rs._
import javax.ws.rs.core.MediaType
import com.factual.driver.{Point, Factual}

/**
 * @author dguardado
 */

case class Location(address : String, lat : Double, long : Double)

@Path("/location")
@Produces(Array(MediaType.APPLICATION_JSON))
@Consumes(Array(MediaType.APPLICATION_JSON))
class LocationResource(factual : Factual) {

  @GET
  def get(@QueryParam("lat") lat :Double, @QueryParam("long") long : Double) : Location = {

    val fact = factual.reverseGeocode(new Point(lat, long)).first()
    val address = fact.get("address") + " " +
      fact.get("locality") + ", " + fact.get("region") + " " + fact.get("country") + " " +
      fact.get("postcode") + "-" + fact.get("plus4")

    new Location(address,
      fact.get("latitude").asInstanceOf[Double],
      fact.get("longitude").asInstanceOf[Double])
  }

}
