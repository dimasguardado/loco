/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
package com.humandriven.loco

import reflect.BeanProperty

/**
 * @author dguardado
 */
class FactualConfig {

  @BeanProperty
  var key = "key"

  @BeanProperty
  var secret = "secret"

}
