/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
package com.humandriven.loco

import com.yammer.dropwizard.config.Configuration
import reflect.BeanProperty

/**
 * @author dguardado
 */
class LocoConfig extends Configuration {

  @BeanProperty
  var factual = new FactualConfig

}
