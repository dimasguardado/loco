/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
package com.humandriven.loco

import com.yammer.dropwizard.ScalaService
import com.yammer.dropwizard.config.Environment
import com.yammer.dropwizard.bundles.AssetsBundle
import com.factual.driver.Factual

/**
 * @author dguardado
 */
object LocoService extends ScalaService[LocoConfig]("Loco") {
  addBundle(new AssetsBundle("/assets/", AssetsBundle.DEFAULT_CACHE_SPEC, "/"))

  def initialize(config: LocoConfig, environment: Environment) {
    val factual = new Factual(config.getFactual().getKey(), config.getFactual().getSecret())
    environment.addResource(new LocationResource(factual))
    environment.addHealthCheck(new FactualHealthCheck(factual))
  }
}
