/*
 * Copyright (c) Dimas Guardado, Jr. All Rights Reserved
 */
package com.humandriven.loco

import com.yammer.metrics.core.HealthCheck
import com.factual.driver.{Query, Factual}
import com.yammer.metrics.core.HealthCheck.Result

/**
 * @author dguardado
 */
class FactualHealthCheck(factual : Factual) extends HealthCheck("factual") {

  def check() : Result = {
    try {
      Result.healthy(factual.fetch("places", new Query().limit(1)).first().get("name").toString)
    } catch {
      case e : Exception => Result.unhealthy(e)
    }
  }

}
